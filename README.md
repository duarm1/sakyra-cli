# Sakyra CLI

The UI/CLI tool for sakyra (CLI only for now)

create a sakyra.lua at the root of your sakyra project

sakyra.lua
```
return {
	project_name = "Project",
	project_short_name = "proj",
	generated_data_folder = "proj/data/",
	assets_root = "proj/assets/",
	textures_root = "sprites/",
	sound_root = "sound/",
	font_root = "fonts/",
	music_root = "music/",
	data_root = "data/",
}
```


```shell
$ sakyra new test # creates new sakyra project
$ sakyra conv qoi # converts every png to qoi
$ sakyra gen # generates .h files for textures,audio,fonts to generated_data_folder
```
